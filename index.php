<?php
session_start();
require_once('./config.php');
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
</head>
<body>
</body>
<h2>Test sprawdzający wiedzę z PPP</h2><br />

<?php
    if(!isset($_GET['token'])) {
        die("Nie podano tokena użytkownika dostęp do testu niemożliwy");
    }
    $conn = new mysqli(HOST, USER, PASS, DB);
    if($conn->connect_errno) {
        die("Nie udało się nawiązać połączenia z bazą danych");
    }
    $token = $conn->real_escape_string($_GET['token']);
    $check_query = "SELECT * FROM users WHERE hash = '".$token."' LIMIT 1";

    $result = $conn->query($check_query);
    print $conn->error;
    if($result->num_rows < 1) {
        die("Nie udało się pobrać danych użytkownika z bazy");
    }
    $user_data = $result->fetch_array();
    $query_q = "SELECT * FROM questions";
    $result_q = $conn->query($query_q);
    $i = 0;
    while($result_q->fetch_array())
        $i++;
    if($user_data['test_done'] == 1) {
        print("Użytkownik ".$user_data['name'].' '.$user_data['surname'].' wykonał już test z wynikiem: '.$user_data['score'].'pkt./'.$i.'pkt.  <br />Do testu można podejść tylko raz');
        die();
    }

    $query = "SELECT * FROM questions";
    $result = $conn->query($query);
    if($result->num_rows < 0) {
        die("Nie udało się pobrać pytań z bazy danych");
    } 
    if(!isset($_GET['start'])) {
        print('<div style="color: red; width: 100%; text-align: center;">UWAGA! Test można wykonać tylko raz. Po naciśnięciu zakończ test nie będzie możliwości ponownego wykonania<br /><br /></div>');
        print('<div style="color: red; width: 100%; text-align: center">Na wykonanie testu jest 1 godzina. Po naciśnięciu klawisza START rozpocznie się odliczanie czasu<br />Po upływie czasu test wyśle się samoczynnie<br /><a href="index.php?token='.$_GET['token'].'&start=1" style="color: black; font-size: 20pt">START</a></div>');
    } else {
        if(!isset($_SESSION['czas'])) {
            //dodać limit czasu
            $_SESSION['czas'] = time()+3601;
        }
        $limit = $_SESSION['czas'] - time();
        ?>
        <div id="count"></div>
        <style>
          #count {
            position: fixed;
            font-size: 18pt;
            top: 1em;
            right: 1em;
          }
        </style>
        <script type="text/javascript">
            function zfill(num, len) {return (Array(len).join("0") + num).slice(-len);}
            var count = <?php print $limit;?>;
            var interval = setInterval(function(){
                var czas = 'Czas pozostały do końca: '+String(zfill(Math.floor((count-1)/60),2))+':'+String(zfill((count-1)%60,2));
                document.getElementById('count').innerHTML=czas;
                count--;
                if (count === 0){
                    clearInterval(interval);
                 document.getElementById('count').innerHTML='Koniec czasu';
                // or...
                alert("Czas się skończył test zostanie wysłany");
                document.getElementById('formularz').submit();
                }
            }, 1000);
        </script>
        <?php
        print('<form method="post" action="process.php" id="formularz">');
        print('<input type="hidden" name="token" value="'.$token.'"/>');
        $i = 1;
        $pytania = array();
        while($pytanie = $result->fetch_array()) {
            $pytania[] = $pytanie;
        }
        //tasowanie pytań
        shuffle($pytania);
        foreach($pytania as $pytanie) {
            //tworzenie formularza z pytaniem
            print("Pytanie nr ".$i++."<br />");
            print($pytanie['question'].'<br />');
            $answers = unserialize(base64_decode($pytanie['answers']));

            //wypisywanie odpowiedzi na pytanie
            $j = 1;
            foreach($answers as $odp) {
                print('<input type="radio" name="odpowiedzi['.$pytanie['q_id'].']" value="'.$j.'">');
                print('<label for="'.$j++.'">'.$odp.'</label><br />');
            }
            print('<hr />');
        }
        print('<input type="submit" name="submitbtn" value="Zakończ test" /></form>');
    }
?>
</html>