<?php
require_once('./config.php');
$conn = new mysqli(HOST, USER, PASS, DB);
$wynik = 0;
if($conn->connect_errno) {
    die("Nie udało się połączyć z bazą danych");
}
$query = "SELECT * FROM questions";
$result = $conn->query($query);

if(!$result) {
    die("Błąd podczas sprawdzania pytań");
}
$pytania = array();
while($pytanie = $result->fetch_array()) {
    $pytania[$pytanie['q_id']] = $pytanie;
}
$max_wynik = count($pytania);
foreach($_POST['odpowiedzi'] as $q_id => $odpowiedz) {
    if($pytania[$q_id]['correct_answer'] == $odpowiedz)
        $wynik++;
} 

$query = 'UPDATE users SET test_done = 1, score='.$wynik.' WHERE hash=\''.$_POST['token'].'\', answers=\''.base64_encode(serialize($_POST['odpowiedzi'])).'\' LIMIT 1';
$conn->query($query);
if($conn->errno) {
    print $query.'<br />';
    print $conn->error.'<br />';
    print ("Nie udało się zapisać wyniku do bazy danych<br />");
}

print("Twój wynik to: ".$wynik.'pkt/'.$max_wynik.'pkt');
?>