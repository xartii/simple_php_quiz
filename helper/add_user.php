<?php
require_once('../config.php');

if(isset($_POST['submit'])) {
    $conn = new mysqli(HOST, USER, PASS, DB);

    if($conn->connect_errno) {
        die("Błąd podczas łączenia z bazą danych");
    }
    $i = 0;
    foreach($_POST['name'] as $imie) {
        $imie = $conn->real_escape_string($imie);
        $nazwisko = $conn->real_escape_string($_POST['surname'][$i]);
        $email = $conn->real_escape_string($_POST['mail'][$i++]);
        $hash = sha1($imie.$nazwisko.$email);
        $query = "INSERT INTO users (name, surname, email, hash) VALUES ('".$imie."', '".$nazwisko."', '".$email."', '".$hash."')";
        if(!$result = $conn->query($query)) {
            print $query;
            print $conn->error;
            print ("Nie udało się dodać użytkownika ".$imie." ".$nazwisko.'<br />');
        } else {
            print ("Dodano użytkownika ".$imie." ".$nazwisko.'<br />');
        }
    }
}
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <script type="text/javascript">
        function addInput(divName){
          var newdiv = document.createElement('div');
          newdiv.innerHTML = '<div class="grid-container"><div class="grid-child">Imię:<br /><input type="text" name="name[]"></div><div class="grid-child">Nazwisko:<br /><input type="text" name="surname[]" /></div><div class="grid-child">E-mail:<br /><input type="text" name="mail[]" /></div></div>';
          document.getElementById(divName).appendChild(newdiv);
        }
    </script>
    <style>
        .grid-container {
            display: grid;
            grid-template-columns: 1fr 1fr 1fr;
            grid-gap: 20px;
        }
    </style>
</head>
<body>
Dodawanie użytkowników<br />
<form method="post">
<div id="dane" style="width: 600px">
    <div class="grid-container">
        <div class="grid-child">Imię:<br /><input type="text" name="name[]" /></div>
        <div class="grid-child">Nazwisko:<br /><input type="text" name="surname[]" /></div>
        <div class="grid-child">E-mail:<br /><input type="text" name="mail[]" /></div>
    </div>
</div><br />
<button type="button" onClick="addInput('dane')">Dodaj kolejną osobę</button><br /><br />
<input type="submit" name="submit" value="Zapisz" />
</form>
</body>
</html>