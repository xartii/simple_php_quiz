<!doctype html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<?php
    require('../config.php');
    $conn = new mysqli(HOST, USER, PASS, DB);
    if($conn->errno) {
        die("Nie udało się połączyć z bazą danych");
    }

    $query = "SELECT * FROM users";
    $result = $conn->query($query);
    $query_q = "SELECT * FROM questions";
    $questions = array();
    $max_wynik = count($questions);
    $result_q = $conn->query($query_q);
    while($qu = $result_q->fetch_array()) {
        $questions[$qu['q_id']] = $qu;
    }
    if($conn->error) {
        die("Nie udało się pobrać wyników z bazy danych");
    }
    print("<table style=\"text-align: center; border: 1px solid #000000;\"><tr><td style=\"border: 1px solid #000000;\">L.p</td><td style=\"border: 1px solid #000000;\">Imię i nazwisko</td><td style=\"border: 1px solid #000000;\">Test ukończony</td><td style=\"border: 1px solid #000000;\">Wynik</td><td style=\"border: 1px solid #000000;\">Odpowiedzi na pytania</td></tr>");
    $i = 1;
    while($uzytkownik = $result->fetch_array()) {
        if($uzytkownik['test_done'] == 1) {
            $ukonczony = 'TAK';
        } else {
            $ukonczony = 'NIE';
        }
        $odpowiedzi = unserialize(base64_decode($uzytkownik['answers']));
        $odp_string = '<table style="text-align: left">';
        if($uzytkownik['test_done'] == 0) {
            $odp_string .= 'BRAK';
        } else {
            foreach($odpowiedzi as $key => $value) {
                $odp_pytanie = unserialize(base64_decode($questions[$key]['answers']));
                $odp_string .= '<tr><td>'.$questions[$key]['question'].'</td><td>';
                if($value == $questions[$key]['correct_answer']) {
                    $odp_string .= '<font style="color: green">'.$odp_pytanie[$value-1].'</font></td></tr>';
                } else {
                    $odp_string .= '<font style="color: red">'.$odp_pytanie[$value-1].'</font></td></tr>';
                }
            }
        }
        $odp_string .= '</table>';
        print('<tr><td style="border: 1px solid #000000;">'.$i++.'</td><td style="border: 1px solid #000000;">'.$uzytkownik['name'].' '.$uzytkownik['surname'].'</td><td style="border: 1px solid #000000;">'.$ukonczony.'</td><td style="border: 1px solid #000000;">'.$uzytkownik['score'].'</td><td style="border: 1px solid #000000;">'.$odp_string.'</td></tr>');
    }
    print('</table>')
?>

</body>
</html>