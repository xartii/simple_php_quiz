<?php
require_once('../config.php');
if(isset($_POST['submit'])) {
    $conn = new mysqli(HOST, USER, PASS, DB);

    if($conn->connect_errno) {
        die("Nie udało się nawiązać połączenia z bazą danych");
    }
    $pytanie = $conn->real_escape_string($_POST['query']);
    $i = 0;
    $odpowiedzi = array();
    foreach($_POST['answers'] as $ans) {
        $odpowiedzi[$i]  = $conn->real_escape_string($ans);
        if(strlen(trim($odpowiedzi[$i])) == 0) {
            die("Odpowiedź nie może być pusta");
        }
        $i++;
    }
    $poprawna = intval($_POST['correct_answer']);
    if(strlen(trim($pytanie)) == 0) {
        die("Pytanie nie może być puste");
    }
    if($poprawna == 0) {
        die("Błędnie podano numer poprawnej odpowiedzi");
    }
    $query = "INSERT INTO questions (question, answers, correct_answer) VALUES ('".$pytanie."', '".base64_encode(serialize($odpowiedzi))."', '".$poprawna."')";
    if(!$result = $conn->query($query)) {
        die("Nie udało się zapisać pytania");
    } 
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script type="text/javascript">
        var counter = 4;
        function addInput(divName){
          var newdiv = document.createElement('div');
          newdiv.innerHTML = counter+++'. <input type="text" name="answers[]" style="width: 400px">';
          document.getElementById(divName).appendChild(newdiv);
        }
    </script>
</head>
<body>
    <?php
    if($result) {
        print("Pytanie zostało zapisane<br />");
    }
    ?>
    <form method="post">
    <label for="query">Pytanie:</label><br />
    <input type="text" name="query" style="width: 400px"><br />
    <label for="correct_answer">Numer poprawnej odpowiedzi:</label><br />
    <input type="text" name="correct_answer" style="width: 400px"/><br /><br />
    <div id="odpowiedzi">
        Odpowiedzi:<br />
        1. <input type="text" name="answers[]" style="width: 400px"><br />
        2. <input type="text" name="answers[]" style="width: 400px"><br />
        3. <input type="text" name="answers[]" style="width: 400px"><br />
    </div>
    <br /><button type="button" onClick="addInput('odpowiedzi')">Dodaj kolejną odpowiedź</button><br /><br />
    <input type="submit" name="submit" value="Dodaj pytanie" />
    </form>
</body>
</html>