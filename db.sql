DROP DATABASE IF EXISTS quiz;
CREATE DATABASE quiz;
USE quiz;

CREATE TABLE questions (
    q_id int not null AUTO_INCREMENT,
    question varchar(2000) not null,
    answers varchar(2000) not null,
    correct_answer tinyint not null,
    PRIMARY KEY(q_id)
);

CREATE TABLE users (
    u_id int not null AUTO_INCREMENT,
    name varchar(2000) not null,
    surname varchar(2000) not null,
    email varchar(2000) not null,
    hash char(20) not null,
    test_done tinyint(1) not null default 0,
    score tinyint not null default 0,
    answers varchar(4000),
    PRIMARY KEY(u_id)
);